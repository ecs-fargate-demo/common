variable "region" {
  default = "ap-southeast-2"
}

variable "access_key" {}
variable "secret_key" {}

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

variable "infrastructure_tags" {
  type  = "map"
  default = {
    "service" = "production"
    "type"    = "infrastructure"
    "owner"   = "shared-service"
  }
}
